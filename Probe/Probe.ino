// PLEASE CHECK MY CODE I AM NOT AN EXPERT AND I THINK SOME
// OF IT DOESN'T QUITE WORK

#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2591.h>

int pinOne = 8;
int pinTwo = 9;

const int num = 20;

int timer = 0;

uint16_t light;
int counter = 0;
uint16_t vals[num];
uint16_t avg;

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);

void get_lights();

void setup()
{
  Serial.begin(9600);
  pinMode(pinOne, OUTPUT);
  pinMode(pinTwo, OUTPUT);
  tsl.enable();
  get_lights();
  average(vals);
}

void loop()
{
  update_vals();
  
  if (light > 1.5 * avg) // Checks if current light point is 1.5x greater than current average (low, without light point)
  {
    get_lights(); // Gets values at current light state for comparison
    timer = millis();
    while (millis() - timer < 2000) // Used as a timer, basically
    {
      update_vals();
      
      if (abs(avg - light) > 300) // Checks if current light point is far less than the average (high, but with light point)
      {
        get_lights();
        timer = millis();
        while (millis() - timer < 2000)
        {
          update_vals();
          
          if (light > 1.5 * avg) // Repeats previous checks, basically
          {
            get_lights();
            timer = millis();
            while (millis() - timer < 2000)
            {
              update_vals();
              
              if (abs(avg - light) > 300)
              {
                Serial.println("On");
                digitalWrite(pinTwo, LOW);
                digitalWrite(pinOne, HIGH);
                delay(10 * 1000);
                digitalWrite(pinOne, LOW);
                digitalWrite(pinTwo, HIGH);
                delay(10 * 1000);
                digitalWrite(pinOne, LOW);
                digitalWrite(pinTwo, LOW);
              }
            }
          }
        }
      }
    }
  }
}

void update_vals() // Captures a single light value and stores it in vals
{
  average(vals); // Get average of vals
  counter = counter % num; // Makes sure counter is less than num
  light = tsl.getLuminosity(TSL2591_VISIBLE); // Captures a light value
  Serial.println(avg); // Debug stuff
  Serial.println(light);
  Serial.println();
  vals[counter] = light; // Stores light value in vals
  counter++;
}

void get_lights() // Captures num amount of light values and stores them in vals
{
  for (int i = 0; i < num; i++)
  {
    vals[i] = tsl.getLuminosity(TSL2591_VISIBLE);
  }
  counter = 0;
}

uint16_t average(uint16_t vals[]) // Calculates average of input array
{
  uint16_t sum = 0;
  for (int i = 0; i < num; i++)
  {
    sum += vals[i];
  }
  avg = sum / num;
}
