#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2591.h>

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);

int pinOne = 8;
int pinTwo = 9;
const float sequence[] = {3, 10, 2, 3, 2}; // Sum to num-1, 
const int len = int(sizeof(sequence) / sizeof(float));
float timing[len]; // Array to store lengths of flashes
const int num = 21; // Number of light inputs to take.
int vals[num]; // Array to store light values
float times[num]; // Array that enumerates light values for use in getting length of flashes

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinOne, OUTPUT);
  pinMode(pinTwo, OUTPUT);
  Serial.begin(9600);
  tsl.enable();
  tsl.setGain(TSL2591_GAIN_HIGH); // Sets light sensor to be more sensitive
  tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); // Sets light sensor to take quick values, at the expense of sensitivity.
  digitalWrite(LED_BUILTIN, HIGH); // Turns on an LED so that human operator can prepare to start light sequence.
  delay(3000); // Delays for 3 seconds so that it's relatively easy to get the timing right
  digitalWrite(LED_BUILTIN, LOW); // Turns LED off a second before light collection occurs. 
  delay(1000); // All the previous code is subject to change if I find a better way to sync up two Arduinos somehow, or it will be moved into loop() if I can't
  get_vals();
  process_vals();
  if (check_vals()) // Turns on the motors on the probe when the lights match up. 
  {
    Serial.println("On");
    digitalWrite(pinTwo, LOW);
    digitalWrite(pinOne, HIGH);
    delay(10 * 1000);
    digitalWrite(pinOne, LOW);
    digitalWrite(pinTwo, HIGH);
    delay(10 * 1000);
    digitalWrite(pinOne, LOW);
    digitalWrite(pinTwo, LOW);
  }
}

void loop()
{}

// Simply gets light values and stores them
void get_vals()
{
  for (int i = 0; i < num; i++)
  {
    vals[i] = tsl.getLuminosity(TSL2591_VISIBLE);
    times[i] = i + 1;
  }
}

// Takes light values and measures the length of periods of on and off
void process_vals()
{
  int current_time = times[0]; // Assumes a period of light or dark starts with the first value. Could cause issues, see next comment.
  int time_counter = 0; // Just keeps track of place in the timing array.
  for (int i = 1; i < num; i++)
  {
    if (abs(vals[i] - vals[i - 1]) > 0.8) // Checks for a jump or dip in light intensity - this marks transitions between on and off
    {                                     // Also means the light has to be on before the program starts checking light values or else
      Serial.print("Time: ");             // it's going to be wrong. Will try to work on that.
      Serial.println(times[i]);
      timing[time_counter] = times[i] - current_time + 1; // Measures number of light values between transitions, i.e. the length of a certain period of on or off
      Serial.println(timing[time_counter]);
      current_time = times[i]; // Jumps to the start of a period of on or off, so that it can be measured.
      time_counter++;
    }
  }
}

// Checks if the measured light flashes match the sequence
bool check_vals()
{
  for (int i = 0; i < len; i++)
  {
    if (abs(sequence[i] - timing[i]) > 1) // Just checks if the difference between the proper sequence and the measured sequence is too large for them to be close enough.
    {
      return false;
    }
  }
  return true;
}
